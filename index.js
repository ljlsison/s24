console.log("howdy")

const getCube = (num) => {
	let cubed = num**3
	console.log(`The cube of ${num} is ${cubed}`)
	}
getCube(3)

const address = [
	'258',
	'Washington Ave NW',
	'California',
	'90011'
]

let [houseNumber,street,state,zip] = address

console.log(`I live at ${houseNumber} ${street}, ${state} ${zip}`)

const animal = {
	name: 'Lolong',
	species: 'saltwater crocodile',
	weight: '1075 kgs',
	measurement: '20 ft 3 in'
}

let {name,species,weight, measurement} = animal

console.log(`${name} was a ${species}. He weighed ${weight} with a measurement of ${measurement}.`);

let arr = [1,2,3,4,5]

arr.forEach((num) => console.log(num));

let reduceNumber = arr.reduce((a,b) => a + b);

console.log(reduceNumber)



